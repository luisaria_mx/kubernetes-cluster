data "oci_identity_availability_domains" "ADs" {
  compartment_id = "${var.tenancy_ocid}"
}

resource "template_file" "etcd_discovery_url" {
  provisioner "local-exec" {
    command = "[ -d ${path.root}/generated ] || mkdir -p ${path.root}/generated && curl https://discovery.etcd.io/new?size=${var.k8smaster_ad1_count + var.k8smaster_ad2_count + var.k8smaster_ad3_count} > ${path.root}/generated/discovery${self.id}"
  }
}
