module "k8s-tls" {
  source                 = "./tls"
  api_server_private_key = "${var.api_server_private_key}"
  api_server_cert        = "${var.api_server_cert}"
  ca_cert                = "${var.ca_cert}"
  ca_key                 = "${var.ca_key}"
  api_server_admin_token = "${var.api_server_admin_token}"
  master_lb_public_ip    = "${module.k8smaster-public-lb.ip_addresses[0]}"
  ssh_private_key        = "${var.ssh_private_key}"
  ssh_public_key         = "${var.ssh_public_key}"
}

module "vcn" {
  source              = "./network/vcn"
  tenancy_ocid        = "${var.tenancy_ocid}"
  vcn_cidr            = "${var.vcn_cidr}"
  compartment_ocid    = "${var.compartment_ocid}"
  label_prefix        = "${var.label_prefix}"
  k8svcn_dns_name     = "${var.k8svcn_dns_name}"
}

module "k8smasterAD1" {
  source                          = "./instances/k8smaster"
  count                           = "${var.k8smaster_ad1_count}"
  tenancy_ocid                    = "${var.tenancy_ocid}"
  compartment_ocid                = "${var.compartment_ocid}"
  availability_domain             = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[0], "name")}"
  k8smaster_hostname_label_prefix = "${var.k8smaster_hostname_label_prefix}"
  k8smaster_image_name            = "${var.k8smaster_image_name}"
  k8smaster_instance_shape        = "${var.k8smaster_instance_shape}"
  ssh_public_key                  = "${module.k8s-tls.ssh_public_key}"
  subnet_id                       = "${module.vcn.k8smaster_subnet_ad1_id}"
  etcd_discovery_url              = "${template_file.etcd_discovery_url.id}"
  flannel_backend                 = "VXLAN"
  flannel_network_cidr            = "${var.flannel_network_cidr}"
  flannel_network_subnetlen       = 24
  domain_name                     = "${var.domain_name}"
  api_server_cert_pem             = "${module.k8s-tls.api_server_cert_pem}"
  api_server_count                = "${var.k8smaster_ad1_count + var.k8smaster_ad2_count + var.k8smaster_ad3_count}"
  api_server_private_key_pem      = "${module.k8s-tls.api_server_private_key_pem}"
  k8s_apiserver_token_admin  = "${module.k8s-tls.api_server_admin_token}"
  docker_ver                 = "${var.docker_ver}"
  master_docker_max_log_size = "${var.master_docker_max_log_size}"
  master_docker_max_log_files = "${var.master_docker_max_log_files}"
  etcd_discovery_url         = "${template_file.etcd_discovery_url.id}"
  etcd_ver                   = "${var.etcd_ver}"
  flannel_ver                = "${var.flannel_ver}"
  k8s_dashboard_ver          = "${var.k8s_dashboard_ver}"
  k8s_dns_ver                = "${var.k8s_dns_ver}"
  k8s_ver                    = "${var.k8s_ver}"
  root_ca_pem                = "${module.k8s-tls.root_ca_pem}"
  etcd_endpoints             = "${join(",",formatlist("http://%s:2379", module.k8setcd-lb.ip_addresses))}"
}

module "k8smasterAD2" {
  source                          = "./instances/k8smaster"
  count                           = "${var.k8smaster_ad2_count}"
  tenancy_ocid                    = "${var.tenancy_ocid}"
  compartment_ocid                = "${var.compartment_ocid}"
  availability_domain             = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[1], "name")}"
  k8smaster_hostname_label_prefix = "${var.k8smaster_hostname_label_prefix}"
  k8smaster_image_name            = "${var.k8smaster_image_name}"
  k8smaster_instance_shape        = "${var.k8smaster_instance_shape}"
  ssh_public_key                  = "${module.k8s-tls.ssh_public_key}"
  subnet_id                       = "${module.vcn.k8smaster_subnet_ad2_id}"
  etcd_discovery_url              = "${template_file.etcd_discovery_url.id}"
  flannel_backend                 = "VXLAN"
  flannel_network_cidr            = "${var.flannel_network_cidr}"
  flannel_network_subnetlen       = 24
  domain_name                     = "${var.domain_name}"
  api_server_cert_pem             = "${module.k8s-tls.api_server_cert_pem}"
  api_server_count                = "${var.k8smaster_ad1_count + var.k8smaster_ad2_count + var.k8smaster_ad3_count}"
  api_server_private_key_pem      = "${module.k8s-tls.api_server_private_key_pem}"
  k8s_apiserver_token_admin  = "${module.k8s-tls.api_server_admin_token}"
  docker_ver                 = "${var.docker_ver}"
  master_docker_max_log_size = "${var.master_docker_max_log_size}"
  master_docker_max_log_files = "${var.master_docker_max_log_files}"
  etcd_discovery_url         = "${template_file.etcd_discovery_url.id}"
  etcd_ver                   = "${var.etcd_ver}"
  flannel_ver                = "${var.flannel_ver}"
  k8s_dashboard_ver          = "${var.k8s_dashboard_ver}"
  k8s_dns_ver                = "${var.k8s_dns_ver}"
  k8s_ver                    = "${var.k8s_ver}"
  root_ca_pem                = "${module.k8s-tls.root_ca_pem}"
  etcd_endpoints             = "${join(",",formatlist("http://%s:2379", module.k8setcd-lb.ip_addresses))}"
}

module "k8smasterAD3" {
  source                          = "./instances/k8smaster"
  count                           = "${var.k8smaster_ad3_count}"
  tenancy_ocid                    = "${var.tenancy_ocid}"
  compartment_ocid                = "${var.compartment_ocid}"
  availability_domain             = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[2], "name")}"
  k8smaster_hostname_label_prefix = "${var.k8smaster_hostname_label_prefix}"
  k8smaster_image_name            = "${var.k8smaster_image_name}"
  k8smaster_instance_shape        = "${var.k8smaster_instance_shape}"
  ssh_public_key                  = "${module.k8s-tls.ssh_public_key}"
  subnet_id                       = "${module.vcn.k8smaster_subnet_ad3_id}"
  etcd_discovery_url              = "${template_file.etcd_discovery_url.id}"
  flannel_backend                 = "VXLAN"
  flannel_network_cidr            = "${var.flannel_network_cidr}"
  flannel_network_subnetlen       = 24
  domain_name                     = "${var.domain_name}"
  api_server_cert_pem             = "${module.k8s-tls.api_server_cert_pem}"
  api_server_count                = "${var.k8smaster_ad1_count + var.k8smaster_ad2_count + var.k8smaster_ad3_count}"
  api_server_private_key_pem      = "${module.k8s-tls.api_server_private_key_pem}"
  k8s_apiserver_token_admin  = "${module.k8s-tls.api_server_admin_token}"
  docker_ver                 = "${var.docker_ver}"
  master_docker_max_log_size = "${var.master_docker_max_log_size}"
  master_docker_max_log_files = "${var.master_docker_max_log_files}"
  etcd_discovery_url         = "${template_file.etcd_discovery_url.id}"
  etcd_ver                   = "${var.etcd_ver}"
  flannel_ver                = "${var.flannel_ver}"
  k8s_dashboard_ver          = "${var.k8s_dashboard_ver}"
  k8s_dns_ver                = "${var.k8s_dns_ver}"
  k8s_ver                    = "${var.k8s_ver}"
  root_ca_pem                = "${module.k8s-tls.root_ca_pem}"
  etcd_endpoints             = "${join(",",formatlist("http://%s:2379", module.k8setcd-lb.ip_addresses))}"
}

#
# resource "null_resource" "k8s-master-ad1-setup" {
#   count = "${var.k8smaster_ad1_count}"
#
#   triggers = {
#     k8s_master_ad1_ids = "${join(",", module.k8smasterAD1.ids)}"
#   }
#
#   connection {
#     type     = "ssh"
#     user     = "opc"
#     host = "${element(module.k8smasterAD1.public_ips, count.index)}"
#     private_key = "${var.ssh_private_key}"
#   }
#
#   provisioner "remote-exec" {
#     inline = [
#       "sudo /root/setup.preflight.sh ${var.etcd_lb_enabled=="true" ?
#                                     join(",",formatlist("http://%s:2379",
#                                                               module.k8setcd-lb.ip_addresses)):
#                                     join(",",formatlist("http://%s:2379",compact(concat(
#                                                               module.k8smasterAD1.private_ips,
#                                                               module.k8smasterAD2.private_ips,
#                                                               module.k8smasterAD3.private_ips)))) }",
#     ]
#   }
#
#   # provisioner "local-exec" {
#   #   command = "sleep 300"
#   # }
#
# }
#
# resource "null_resource" "k8s-master-ad2-setup" {
#   count = "${var.k8smaster_ad2_count}"
#
#   triggers = {
#     k8s_master_ad1_ids = "${join(",", module.k8smasterAD2.ids)}"
#   }
#
#   connection {
#     type     = "ssh"
#     user     = "opc"
#     host = "${element(module.k8smasterAD2.public_ips, count.index)}"
#     private_key = "${var.ssh_private_key}"
#   }
#
#   provisioner "remote-exec" {
#     inline = [
#       "sudo /root/setup.preflight.sh ${var.etcd_lb_enabled=="true" ?
#                                     join(",",formatlist("http://%s:2379",
#                                                               module.k8setcd-lb.ip_addresses)):
#                                     join(",",formatlist("http://%s:2379",compact(concat(
#                                                               module.k8smasterAD1.private_ips,
#                                                               module.k8smasterAD2.private_ips,
#                                                               module.k8smasterAD3.private_ips)))) }",
#     ]
#   }
#
#   # provisioner "local-exec" {
#   #   command = "sleep 300"
#   # }
#
# }
#
# resource "null_resource" "k8s-master-ad3-setup" {
#   count = "${var.k8smaster_ad3_count}"
#
#   triggers = {
#     k8s_master_ad1_ids = "${join(",", module.k8smasterAD3.ids)}"
#   }
#
#   connection {
#     type     = "ssh"
#     user     = "opc"
#     host = "${element(module.k8smasterAD3.public_ips, count.index)}"
#     private_key = "${var.ssh_private_key}"
#   }
#
#   provisioner "remote-exec" {
#     inline = [
#       "sudo /root/setup.preflight.sh ${var.etcd_lb_enabled=="true" ?
#                                     join(",",formatlist("http://%s:2379",
#                                                               module.k8setcd-lb.ip_addresses)):
#                                     join(",",formatlist("http://%s:2379",compact(concat(
#                                                               module.k8smasterAD1.private_ips,
#                                                               module.k8smasterAD2.private_ips,
#                                                               module.k8smasterAD3.private_ips)))) }",
#     ]
#   }
#
#   # provisioner "local-exec" {
#   #   command = "sleep 300"
#   # }
#
# }

module "k8sworkerAD1" {
  source                          = "./instances/k8sworker"
  count                           = "${var.k8sworker_ad1_count}"
  tenancy_ocid                    = "${var.tenancy_ocid}"
  compartment_ocid                = "${var.compartment_ocid}"
  availability_domain             = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[0], "name")}"
  k8sworker_hostname_label_prefix = "${var.k8sworker_hostname_label_prefix}"
  k8sworker_image_name            = "${var.k8sworker_image_name}"
  k8sworker_instance_shape        = "${var.k8sworker_instance_shape}"
  subnet_id                       = "${module.vcn.k8sworker_subnet_ad1_id}"
  ssh_private_key                 = "${module.k8s-tls.ssh_private_key}"
  ssh_public_key                  = "${module.k8s-tls.ssh_public_key}"
  master_lb                       = "https://${module.k8smaster-public-lb.ip_addresses[0]}:443"
  flannel_backend                 = "VXLAN"
  flannel_network_cidr            = "${var.flannel_network_cidr}"
  flannel_network_subnetlen       = 24
  docker_ver                 = "${var.docker_ver}"
  worker_docker_max_log_size = "${var.worker_docker_max_log_size}"
  worker_docker_max_log_files = "${var.worker_docker_max_log_files}"
  etcd_discovery_url         = "${template_file.etcd_discovery_url.id}"
  etcd_ver                   = "${var.etcd_ver}"
  flannel_ver                = "${var.flannel_ver}"
  k8s_ver                    = "${var.k8s_ver}"
  region                     = "${var.region}"
  api_server_cert_pem        = "${module.k8s-tls.api_server_cert_pem}"
  api_server_private_key_pem = "${module.k8s-tls.api_server_private_key_pem}"
  domain_name                = "${var.domain_name}"
  root_ca_key                = "${module.k8s-tls.root_ca_key}"
  root_ca_pem                = "${module.k8s-tls.root_ca_pem}"
  etcd_endpoints             = "${join(",",formatlist("http://%s:2379", module.k8setcd-lb.ip_addresses))}"
}

module "k8sworkerAD2" {
  source                          = "./instances/k8sworker"
  count                           = "${var.k8sworker_ad2_count}"
  tenancy_ocid                    = "${var.tenancy_ocid}"
  compartment_ocid                = "${var.compartment_ocid}"
  availability_domain             = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[1], "name")}"
  k8sworker_hostname_label_prefix = "${var.k8sworker_hostname_label_prefix}"
  k8sworker_image_name            = "${var.k8sworker_image_name}"
  k8sworker_instance_shape        = "${var.k8sworker_instance_shape}"
  subnet_id                       = "${module.vcn.k8sworker_subnet_ad2_id}"
  ssh_private_key                 = "${module.k8s-tls.ssh_private_key}"
  ssh_public_key                  = "${module.k8s-tls.ssh_public_key}"
  master_lb                       = "https://${module.k8smaster-public-lb.ip_addresses[0]}:443"
  flannel_backend                 = "VXLAN"
  flannel_network_cidr            = "${var.flannel_network_cidr}"
  flannel_network_subnetlen       = 24
  docker_ver                 = "${var.docker_ver}"
  worker_docker_max_log_size = "${var.worker_docker_max_log_size}"
  worker_docker_max_log_files = "${var.worker_docker_max_log_files}"
  etcd_discovery_url         = "${template_file.etcd_discovery_url.id}"
  etcd_ver                   = "${var.etcd_ver}"
  flannel_ver                = "${var.flannel_ver}"
  k8s_ver                    = "${var.k8s_ver}"
  region                     = "${var.region}"
  api_server_cert_pem        = "${module.k8s-tls.api_server_cert_pem}"
  api_server_private_key_pem = "${module.k8s-tls.api_server_private_key_pem}"
  domain_name                = "${var.domain_name}"
  root_ca_key                = "${module.k8s-tls.root_ca_key}"
  root_ca_pem                = "${module.k8s-tls.root_ca_pem}"
  etcd_endpoints             = "${join(",",formatlist("http://%s:2379", module.k8setcd-lb.ip_addresses))}"
}

module "k8sworkerAD3" {
  source                          = "./instances/k8sworker"
  count                           = "${var.k8sworker_ad3_count}"
  tenancy_ocid                    = "${var.tenancy_ocid}"
  compartment_ocid                = "${var.compartment_ocid}"
  availability_domain             = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[2], "name")}"
  k8sworker_hostname_label_prefix = "${var.k8sworker_hostname_label_prefix}"
  k8sworker_image_name            = "${var.k8sworker_image_name}"
  k8sworker_instance_shape        = "${var.k8sworker_instance_shape}"
  subnet_id                       = "${module.vcn.k8sworker_subnet_ad3_id}"
  ssh_private_key                 = "${module.k8s-tls.ssh_private_key}"
  ssh_public_key                  = "${module.k8s-tls.ssh_public_key}"
  master_lb                       = "https://${module.k8smaster-public-lb.ip_addresses[0]}:443"
  flannel_backend                 = "VXLAN"
  flannel_network_cidr            = "${var.flannel_network_cidr}"
  flannel_network_subnetlen       = 24
  docker_ver                 = "${var.docker_ver}"
  worker_docker_max_log_size = "${var.worker_docker_max_log_size}"
  worker_docker_max_log_files = "${var.worker_docker_max_log_files}"
  etcd_discovery_url         = "${template_file.etcd_discovery_url.id}"
  etcd_ver                   = "${var.etcd_ver}"
  flannel_ver                = "${var.flannel_ver}"
  k8s_ver                    = "${var.k8s_ver}"
  region                     = "${var.region}"
  api_server_cert_pem        = "${module.k8s-tls.api_server_cert_pem}"
  api_server_private_key_pem = "${module.k8s-tls.api_server_private_key_pem}"
  domain_name                = "${var.domain_name}"
  root_ca_key                = "${module.k8s-tls.root_ca_key}"
  root_ca_pem                = "${module.k8s-tls.root_ca_pem}"
  etcd_endpoints             = "${join(",",formatlist("http://%s:2379", module.k8setcd-lb.ip_addresses))}"
}

module "k8smaster-public-lb" {
  source                          = "./network/loadbalancers/k8smaster"
  compartment_ocid                = "${var.compartment_ocid}"
  is_private                      = "${var.k8s_master_lb_access == "private" ? "true": "false"}"

  # Handle case where var.k8s_master_lb_access=public, but var.control_plane_subnet_access=private
  k8smaster_subnet_0_id           = "${var.k8s_master_lb_access == "private" ? module.vcn.k8smaster_subnet_ad1_id: join(" ", list(module.vcn.k8smaster_subnet_ad1_id))}"
  k8smaster_subnet_1_id           = "${var.k8s_master_lb_access == "private" ? "": join(" ", list(module.vcn.k8smaster_subnet_ad2_id))}"
  k8smaster_ad1_private_ips       = "${module.k8smasterAD1.private_ips}"
  k8smaster_ad2_private_ips       = "${module.k8smasterAD2.private_ips}"
  k8smaster_ad3_private_ips       = "${module.k8smasterAD3.private_ips}"
  k8smaster_ad1_count             = "${var.k8smaster_ad1_count}"
  k8smaster_ad2_count             = "${var.k8smaster_ad2_count}"
  k8smaster_ad3_count             = "${var.k8smaster_ad3_count}"
  label_prefix                    = "${var.label_prefix}"
  shape                           = "${var.k8sMasterLBShape}"
}

module "k8setcd-lb" {
  source                          = "./network/loadbalancers/etcd"
  count                           = "${var.etcd_lb_enabled=="true"? 1 : 0 }"
  etcd_lb_enabled        = "${var.etcd_lb_enabled}"
  compartment_ocid                = "${var.compartment_ocid}"
  k8smaster_subnet_0_id           = "${module.vcn.k8smaster_subnet_ad1_id}"
  k8smaster_ad1_private_ips       = "${module.k8smasterAD1.private_ips}"
  k8smaster_ad2_private_ips       = "${module.k8smasterAD2.private_ips}"
  k8smaster_ad3_private_ips       = "${module.k8smasterAD3.private_ips}"
  k8smaster_ad1_count             = "${var.k8smaster_ad1_count}"
  k8smaster_ad2_count             = "${var.k8smaster_ad2_count}"
  k8smaster_ad3_count             = "${var.k8smaster_ad3_count}"
  label_prefix                    = "${var.label_prefix}"
  shape                           = "${var.etcdLBShape}"
}

module "kubeconfig" {
  source                     = "./kubernetes/kubeconfig"
  api_server_private_key_pem = "${module.k8s-tls.api_server_private_key_pem}"
  api_server_cert_pem        = "${module.k8s-tls.api_server_cert_pem}"
  k8s_master                 = "https://${module.k8smaster-public-lb.ip_addresses[0]}:443"
}
