# OCI Tenant info variables
variable "tenancy_ocid" {}

variable "compartment_ocid" {}

variable "domain_name" {
  default = "k8soci.oraclevcn.com"
}

variable "user_ocid" {}
variable "fingerprint" {}
variable "private_key_path" {}
variable "private_key_password" {
  default = ""
}

variable "disable_auto_retries" {
  default = "false"
}

variable "region" {
  default = "us-ashburn-1"
}

variable "vcn_cidr" {
  default = "10.0.0.0/16"
}

variable "label_prefix" {
  default = ""
}

variable "k8svcn_dns_name" {
  default = "k8svcn"
}

variable "k8smaster_ad1_count" {
  default = "1"
}

variable "k8smaster_ad2_count" {
  default = "1"
}

variable "k8smaster_ad3_count" {
  default = "1"
}

variable "k8smaster_hostname_label_prefix" {
  default = "k8smaster"
}

variable "k8smaster_image_name" {
  default = "Oracle-Linux-7.4-2017.12.18-0"
}

variable "k8smaster_instance_shape" {
  default = "VM.Standard1.1"
}

variable "k8smaster_subnet_dns_label" {
  default = "k8smaster"
}

variable "k8sworker_ad1_count" {
  default = "1"
}

variable "k8sworker_ad2_count" {
  default = "1"
}

variable "k8sworker_ad3_count" {
  default = "1"
}

variable "k8sworker_hostname_label_prefix" {
  default = "k8sworker"
}

variable "k8sworker_image_name" {
  default = "Oracle-Linux-7.4-2017.12.18-0"
}

variable "k8sworker_instance_shape" {
  default = "VM.Standard1.1"
}

variable "k8sworker_subnet_dns_label" {
  default = "k8sworker"
}

variable "ssh_public_key" {}
variable "ssh_private_key" {}

variable "k8s_master_lb_access" {
  default = "public"
}

variable "k8sMasterLBShape" {
  default = "100Mbps"
}

variable "etcdLBShape" {
  default = "100Mbps"
}

variable "docker_ver" {
  default = "17.06.2.ol"
}

variable "etcd_ver" {
  default = "v3.2.11"
}

variable "k8s_ver" {
  default = "1.8.5"
}

variable "k8s_dashboard_ver" {
  default = "1.8.1"
}

variable "k8s_dns_ver" {
  default = "1.14.7"
}

variable "flannel_ver" {
  default = "v0.9.1"
}

variable "flannel_network_cidr" {
  description = "A CIDR notation IP range to use for the entire flannel network"
  type        = "string"
  default     = "10.99.0.0/16"
}

variable "flannel_network_subnetlen" {
  default = "24"
}

# Docker log file config
variable "etcd_docker_max_log_size" {
  description = "Maximum size of the etcd docker container logs"
  default = "50m"
}

variable "etcd_docker_max_log_files" {
  description = "Maximum number of etcd docker container logs to rotate"
  default = "5"
}

variable "etcd_lb_enabled" {
  default = "true"
}

variable "master_docker_max_log_size" {
  description = "Maximum size of the k8s master docker container json logs"
  default = "50m"
}
variable "master_docker_max_log_files" {
  description = "Maximum number of k8s master docker container json logs to rotate"
  default = "5"
}

variable "worker_docker_max_log_size" {
  description = "Maximum size of the k8s master docker container json logs"
  default = "50m"
}
variable "worker_docker_max_log_files" {
  description = "Maximum number of k8s master docker container json logs to rotate"
  default = "5"
}

variable "api_server_cert" {
  description = "API Server certificate (generated if left blank)"
  type        = "string"
  default     = ""
}

variable "ca_cert" {
  description = "CA certificate (generated if left blank)"
  type        = "string"
  default     = ""
}

variable "ca_key" {
  description = "CA private key (generated if left blank)"
  type        = "string"
  default     = ""
}

variable "api_server_private_key" {
  description = "API Server private key (generated if left blank)"
  type        = "string"
  default     = ""
}

variable "api_server_admin_token" {
  description = "admin user's bearer token for API server (generated if left blank)"
  type        = "string"
  default     = ""
}

variable "k8s_worker_volume_size" {
  default = "200"
}

variable "k8s_master_volume_size" {
  default = "200"
}
