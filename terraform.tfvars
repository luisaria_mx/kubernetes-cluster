# OCI Tenant info
tenancy_ocid = "ocid1.tenancy.oc1..aaaaaaaavswyapsd3prd2ylq34hvilyxfzqy7icvr2v6uq2zvhldq33f7cyq"
compartment_ocid = "ocid1.compartment.oc1..aaaaaaaa4kvguaayh6rdpof3zx4sc3r3d6p35nec7y2agylvp3ecahez6eqa"
fingerprint = "35:35:d0:9a:37:85:93:0a:b0:00:eb:8c:c5:d4:ae:49"
private_key_path = "/home/oracle/.oci/oci_api_key.pem"
user_ocid = "ocid1.user.oc1..aaaaaaaajhbpofsnwsv34xgzyx5x454sqjts4q4z24wi3uh67bgoxyqpuzfa"
region = "us-ashburn-1"

# K8s Masters
k8smaster_ad1_count = "1"
k8smaster_ad2_count = "1"
k8smaster_ad3_count = "0"

k8smaster_instance_shape = "VM.Standard1.2"

# K8s Workers
k8sworker_ad1_count = "1"
k8sworker_ad2_count = "0"
k8sworker_ad3_count = "1"

k8sworker_instance_shape = "VM.Standard1.1"
