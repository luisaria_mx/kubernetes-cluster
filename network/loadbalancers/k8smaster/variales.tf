variable "compartment_ocid" {}
variable "k8smaster_subnet_0_id" {}
variable "k8smaster_subnet_1_id" {}

variable "label_prefix" {
  default = ""
}

variable "k8smaster_ad1_count" {}

variable "k8smaster_ad2_count" {}

variable "k8smaster_ad3_count" {}

variable "k8smaster_ad1_private_ips" {
  type    = "list"
  default = []
}

variable "k8smaster_ad2_private_ips" {
  type    = "list"
  default = []
}

variable "k8smaster_ad3_private_ips" {
  type    = "list"
  default = []
}

variable "shape" {
  default = "100Mbps"
}

variable "is_private" {
  default = "false"
}
