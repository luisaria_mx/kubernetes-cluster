variable "tenancy_ocid" {}

variable "vcn_cidr" {
  default = "10.0.0.0/16"
}

variable "compartment_ocid" {}

variable "label_prefix" {
  default = ""
}

variable "k8svcn_dns_name" {
  default = "k8svcn"
}

variable "k8sig_display_name" {
  default = "k8sig"
}

variable "k8sroutetable_display_name" {
  default = "k8sroutetable"
}

variable "k8smasterseclist_display_name" {
  default = "k8s-master-seclist"
}

variable "k8sworkerseclist_display_name" {
  default = "k8s-worker-seclist"
}

variable "k8smaster_subnet_display_name" {
  default = "k8s-master-subnet"
}

variable "k8sworker_subnet_display_name" {
  default = "k8s-worker-subnet"
}

variable "k8smaster_subnet_dns_label" {
  default = "k8smaster"
}

variable "k8sworker_subnet_dns_label" {
  default = "k8sworker"
}
