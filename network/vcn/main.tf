resource "oci_core_virtual_network" "k8svcn" {
  cidr_block     = "${var.vcn_cidr}"
  compartment_id =   "${var.compartment_ocid}"
  display_name   = "${var.label_prefix}${var.k8svcn_dns_name}"
  dns_label      = "${var.k8svcn_dns_name}"
}

resource "oci_core_internet_gateway" "k8sig" {
    compartment_id = "${var.compartment_ocid}"
    display_name = "${var.k8sig_display_name}"
    vcn_id = "${oci_core_virtual_network.k8svcn.id}"
}

resource "oci_core_route_table" "k8sroutetable" {
    compartment_id = "${var.compartment_ocid}"
    vcn_id = "${oci_core_virtual_network.k8svcn.id}"
    display_name = "${var.k8sroutetable_display_name}"
    route_rules {
        cidr_block = "0.0.0.0/0"
        network_entity_id = "${oci_core_internet_gateway.k8sig.id}"
    }
}

resource "oci_core_security_list" "k8smasterseclist" {
    compartment_id = "${var.compartment_ocid}"
    display_name = "${var.k8smasterseclist_display_name}"
    vcn_id = "${oci_core_virtual_network.k8svcn.id}"
    egress_security_rules = [{
        destination = "0.0.0.0/0"
        protocol = "all"
    }]
    ingress_security_rules = [{
        tcp_options {
            "max" = 22
            "min" = 22
        }
        protocol = "6"
        source = "0.0.0.0/0"
    },
    {
        tcp_options {
            "max" = 443
            "min" = 443
        }
        protocol = "6"
        source = "0.0.0.0/0"
    },
    {
        tcp_options {
            "max" = 32767
            "min" = 30000
        }
        protocol = "6"
        source = "0.0.0.0/0"
    },
  	{
  	   protocol = "all"
  	   source = "${var.vcn_cidr}"
    }]
}

resource "oci_core_security_list" "k8sworkerseclist" {
    compartment_id = "${var.compartment_ocid}"
    display_name = "${var.k8sworkerseclist_display_name}"
    vcn_id = "${oci_core_virtual_network.k8svcn.id}"
    egress_security_rules = [{
        destination = "0.0.0.0/0"
        protocol = "all"
    }]
    ingress_security_rules = [{
        tcp_options {
            "max" = 22
            "min" = 22
        }
        protocol = "6"
        source = "0.0.0.0/0"
    },
    {
        tcp_options {
            "max" = 443
            "min" = 443
        }
        protocol = "6"
        source = "0.0.0.0/0"
    },
    {
        tcp_options {
            "max" = 32767
            "min" = 30000
        }
        protocol = "6"
        source = "0.0.0.0/0"
    },
  	{
  	   protocol = "all"
  	   source = "${var.vcn_cidr}"
    }]
}

resource "oci_core_subnet" "k8smastersubnetAD1" {
  availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[0],"name")}"
  cidr_block = "${element(split(".", var.vcn_cidr),0)}.${element(split(".", var.vcn_cidr),1)}.1.0/24"
  display_name = "${var.k8smaster_subnet_display_name}-ad1"
  dns_label = "${var.k8smaster_subnet_dns_label}ad1"
  compartment_id = "${var.compartment_ocid}"
  vcn_id = "${oci_core_virtual_network.k8svcn.id}"
  route_table_id = "${oci_core_route_table.k8sroutetable.id}"
  security_list_ids = ["${oci_core_security_list.k8smasterseclist.id}"]
  dhcp_options_id = "${oci_core_virtual_network.k8svcn.default_dhcp_options_id}"
}

resource "oci_core_subnet" "k8smastersubnetAD2" {
  availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[1],"name")}"
  cidr_block = "${element(split(".", var.vcn_cidr),0)}.${element(split(".", var.vcn_cidr),1)}.2.0/24"
  display_name = "${var.k8smaster_subnet_display_name}-ad2"
  dns_label = "${var.k8smaster_subnet_dns_label}ad2"
  compartment_id = "${var.compartment_ocid}"
  vcn_id = "${oci_core_virtual_network.k8svcn.id}"
  route_table_id = "${oci_core_route_table.k8sroutetable.id}"
  security_list_ids = ["${oci_core_security_list.k8smasterseclist.id}"]
  dhcp_options_id = "${oci_core_virtual_network.k8svcn.default_dhcp_options_id}"
}

resource "oci_core_subnet" "k8smastersubnetAD3" {
  availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[2],"name")}"
  cidr_block = "${element(split(".", var.vcn_cidr),0)}.${element(split(".", var.vcn_cidr),1)}.3.0/24"
  display_name = "${var.k8smaster_subnet_display_name}-ad3"
  dns_label = "${var.k8smaster_subnet_dns_label}ad3"
  compartment_id = "${var.compartment_ocid}"
  vcn_id = "${oci_core_virtual_network.k8svcn.id}"
  route_table_id = "${oci_core_route_table.k8sroutetable.id}"
  security_list_ids = ["${oci_core_security_list.k8smasterseclist.id}"]
  dhcp_options_id = "${oci_core_virtual_network.k8svcn.default_dhcp_options_id}"
}

resource "oci_core_subnet" "k8sworkersubnetAD1" {
  availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[0],"name")}"
  cidr_block = "${element(split(".", var.vcn_cidr),0)}.${element(split(".", var.vcn_cidr),1)}.4.0/24"
  display_name = "${var.k8sworker_subnet_display_name}-ad1"
  dns_label = "${var.k8sworker_subnet_dns_label}ad1"
  compartment_id = "${var.compartment_ocid}"
  vcn_id = "${oci_core_virtual_network.k8svcn.id}"
  route_table_id = "${oci_core_route_table.k8sroutetable.id}"
  security_list_ids = ["${oci_core_security_list.k8sworkerseclist.id}"]
  dhcp_options_id = "${oci_core_virtual_network.k8svcn.default_dhcp_options_id}"
}

resource "oci_core_subnet" "k8sworkersubnetAD2" {
  availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[1],"name")}"
  cidr_block = "${element(split(".", var.vcn_cidr),0)}.${element(split(".", var.vcn_cidr),1)}.5.0/24"
  display_name = "${var.k8sworker_subnet_display_name}-ad1"
  dns_label = "${var.k8sworker_subnet_dns_label}ad2"
  compartment_id = "${var.compartment_ocid}"
  vcn_id = "${oci_core_virtual_network.k8svcn.id}"
  route_table_id = "${oci_core_route_table.k8sroutetable.id}"
  security_list_ids = ["${oci_core_security_list.k8sworkerseclist.id}"]
  dhcp_options_id = "${oci_core_virtual_network.k8svcn.default_dhcp_options_id}"
}

resource "oci_core_subnet" "k8sworkersubnetAD3" {
  availability_domain = "${lookup(data.oci_identity_availability_domains.ADs.availability_domains[2],"name")}"
  cidr_block = "${element(split(".", var.vcn_cidr),0)}.${element(split(".", var.vcn_cidr),1)}.6.0/24"
  display_name = "${var.k8sworker_subnet_display_name}-ad1"
  dns_label = "${var.k8sworker_subnet_dns_label}ad3"
  compartment_id = "${var.compartment_ocid}"
  vcn_id = "${oci_core_virtual_network.k8svcn.id}"
  route_table_id = "${oci_core_route_table.k8sroutetable.id}"
  security_list_ids = ["${oci_core_security_list.k8sworkerseclist.id}"]
  dhcp_options_id = "${oci_core_virtual_network.k8svcn.default_dhcp_options_id}"
}
