output "id" {
  value = "${oci_core_virtual_network.k8svcn.id}"
}

output "k8smaster_subnet_ad1_id" {
  value = "${oci_core_subnet.k8smastersubnetAD1.id}"
}

output "k8smaster_subnet_ad2_id" {
  value = "${oci_core_subnet.k8smastersubnetAD2.id}"
}

output "k8smaster_subnet_ad3_id" {
  value = "${oci_core_subnet.k8smastersubnetAD3.id}"
}

output "k8sworker_subnet_ad1_id" {
  value = "${oci_core_subnet.k8sworkersubnetAD1.id}"
}

output "k8sworker_subnet_ad2_id" {
  value = "${oci_core_subnet.k8sworkersubnetAD2.id}"
}

output "k8sworker_subnet_ad3_id" {
  value = "${oci_core_subnet.k8sworkersubnetAD3.id}"
}
