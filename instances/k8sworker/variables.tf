variable "tenancy_ocid" {}
variable "availability_domain" {}
variable "compartment_ocid" {}
variable "region" {}
variable "subnet_id" {}

variable "count" {
  default = 1
}

variable "k8sworker_display_name_prefix" {
  default = "k8s-worker"
}

variable "k8sworker_hostname_label_prefix" {
  default = "k8sworker"
}

variable "k8sworker_image_name" {
  default = "Oracle-Linux-7.4-2017.12.18-0"
}

variable "k8sworker_instance_shape" {
  default = "VM.Standard.1.1"
}

variable "k8s_worker_volume_size" {
  default = "200"
}

variable "docker_ver" {
  default = "17.06.2.ol"
}

variable "domain_name" {}
variable "flannel_network_cidr" {}
variable "flannel_network_subnetlen" {}
variable "flannel_backend" {}
variable "etcd_discovery_url" {}

variable "etcd_docker_max_log_size" {
  description = "Maximum size of the etcd docker container json logs"
  default = "50m"
}
variable "etcd_docker_max_log_files" {
  description = "Maximum number of etcd docker container json logs to rotate"
  default = "5"
}

variable "worker_docker_max_log_size" {
  description = "Maximum size of the k8s master docker container json logs"
  default = "50m"
}
variable "worker_docker_max_log_files" {
  description = "Maximum number of k8s master docker container json logs to rotate"
  default = "5"
}

variable "master_lb" {}

variable "etcd_ver" {
  default = "v3.2.11"
}

variable "etcd_endpoints" {}

variable "k8s_ver" {
  default = "1.8.5"
}

variable "k8s_dashboard_ver" {
  default = "1.8.1"
}

variable "k8s_dns_ver" {
  default = "1.14.7"
}

variable "flannel_ver" {
  default = "v0.9.1"
}

variable "ssh_public_key" {}
variable "ssh_private_key" {}

variable "root_ca_pem" {}
variable "root_ca_key" {}
variable "api_server_private_key_pem" {}
variable "api_server_cert_pem" {}
