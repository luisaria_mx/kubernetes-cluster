# resource "null_resource" "sleep" {
#
#   provisioner "local-exec" {
#     command = "sleep 900"
#   }
#
# }
#

resource "oci_core_instance" "k8sworker" {
  count = "${var.count}"
  availability_domain = "${var.availability_domain}"
  compartment_id = "${var.compartment_ocid}"
  display_name = "${var.k8sworker_display_name_prefix}-ad${substr(var.availability_domain, length(var.availability_domain)-1, -1)}-${count.index}"
  hostname_label = "${var.k8sworker_hostname_label_prefix}-ad${substr(var.availability_domain, length(var.availability_domain)-1, -1)}-${count.index}"
  image = "${lookup(data.oci_core_images.k8sworker_image.images[0], "id")}"
  shape = "${var.k8sworker_instance_shape}"
  subnet_id = "${var.subnet_id}"
  extended_metadata {
    roles               = "nodes"
    ssh_authorized_keys = "${var.ssh_public_key}"

    # Automate worker instance configuration with cloud init run at launch time
    user_data = "${data.template_cloudinit_config.master.rendered}"
    tags      = "group:k8s-worker"
  }

  provisioner "remote-exec" {
    when = "destroy"

    inline = [
      "nodeName=`getent hosts $(/usr/sbin/ip route get 1 | awk '{print $NF;exit}') | awk '{print $2}'`",
      "[ -e /usr/bin/kubectl ] && sudo kubectl --kubeconfig /etc/kubernetes/manifests/worker-kubeconfig.yaml drain $nodeName --force",
      "[ -e /usr/bin/kubectl ] && sudo kubectl --kubeconfig /etc/kubernetes/manifests/worker-kubeconfig.yaml delete node $nodeName",
      "exit 0",
    ]

    on_failure = "continue"

    connection {
      host        = "${self.public_ip}"
      user        = "opc"
      private_key = "${var.ssh_private_key}"
      agent       = false
      timeout     = "30s"
    }
  }

  timeouts {
    create = "60m"
  }

  # depends_on = [ "null_resource.sleep" ]

}


resource "oci_core_volume" "k8sworkerVolume" {
  count               = "${var.count}"
  availability_domain = "${var.availability_domain}"
  compartment_id = "${var.compartment_ocid}"
  display_name = "${var.k8sworker_hostname_label_prefix}-ad${substr(var.availability_domain, length(var.availability_domain)-1, -1)}-${count.index}-vol"
  size_in_gbs = "${var.k8s_worker_volume_size}"
}

resource "oci_core_volume_attachment" "k8sworkerVolumeAttach" {
    count           = "${var.count}"
    attachment_type = "iscsi"
    compartment_id = "${var.compartment_ocid}"
    instance_id = "${element(oci_core_instance.k8sworker.*.id, count.index)}"
    volume_id = "${element(oci_core_volume.k8sworkerVolume.*.id, count.index)}"
}
