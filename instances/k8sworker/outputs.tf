output "ids" {
  value = ["${oci_core_instance.k8sworker.*.id}"]
}

output "private_ips" {
  value = ["${oci_core_instance.k8sworker.*.private_ip}"]
}

output "public_ips" {
  value = ["${oci_core_instance.k8sworker.*.public_ip}"]
}

output "instance_host_names" {
  value = ["${oci_core_instance.k8sworker.*.display_name}"]
}
