#!/bin/bash -x

EXTERNAL_IP=$(curl -s -m 10 http://whatismyip.akamai.com/)
ETCD_ENDPOINTS=${etcd_endpoints}

mkdir -p /etc/kubernetes/auth /etc/kubernetes/manifests/

echo "ETCD endpoints: $ETCD_ENDPOINTS" | tee -a /root/setup.log

bash -x /root/setup.sh $ETCD_ENDPOINTS | tee -a /root/setup.log
