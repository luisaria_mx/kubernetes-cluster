# /bin/bash -x

# Turn off SELinux
setenforce 0

# Set working dir
cd /home/opc

# enable ol7 addons
yum-config-manager --disable ol7_UEKR3
yum-config-manager --enable ol7_addons ol7_latest ol7_UEKR4 ol7_optional ol7_optional_latest

# Drop firewall rules
iptables -F

# download etcd
while ! curl -L https://github.com/coreos/etcd/releases/download/${etcd_ver}/etcd-${etcd_ver}-linux-amd64.tar.gz -o /tmp/etcd-${etcd_ver}-linux-amd64.tar.gz; do
	sleep 1
	((secs++)) && ((secs==10)) && break
	echo "Try again"
done

tar zxf /tmp/etcd-${etcd_ver}-linux-amd64.tar.gz -C /tmp/ && cp /tmp/etcd-${etcd_ver}-linux-amd64/etcd* /usr/local/bin/

# add /usr/local/bin to PATH
echo 'PATH=/usr/local/bin/:$PATH' >> /etc/bashrc

# Get IP Address of self
export IP_LOCAL=$(ip route show to 0.0.0.0/0 | awk '{ print $5 }' | xargs ip addr show | grep -Po 'inet \K[\d.]+')
export SUBNET=$(getent hosts $IP_LOCAL | awk '{print $2}' | cut -d. -f2)

export HOSTNAME=$(hostname)
export FQDN_HOSTNAME="$(getent hosts $IP_LOCAL | awk '{print $2}')"

mkdir -p /var/lib/etcd/

# Create etcd cluster node
# nohup /usr/local/bin/etcd --name $HOSTNAME \
#                           --data-dir /var/lib/etcd \
#                           --advertise-client-urls http://$IP_LOCAL:2379 \
#                           --initial-advertise-peer-urls http://$IP_LOCAL:2380 \
#                           --listen-client-urls http://$IP_LOCAL:2379,http://127.0.0.1:2379 \
#                           --listen-peer-urls http://0.0.0.0:2380 \
#                           --discovery ${etcd_discovery_url} >> /home/opc/etcd.log 2>&1 &
envsubst </root/services/etcd.service >/etc/systemd/system/etcd.service
systemctl daemon-reload && systemctl enable etcd && systemctl start etcd

# Generate a flannel configuration JSON that we will store into etcd using curl.
cat >/tmp/flannel-network.json <<EOF
{
  "Network": "${flannel_network_cidr}",
  "Subnetlen": ${flannel_network_subnetlen},
  "Backend": {
    "Type": "${flannel_backend}",
    "VNI" : 1
  }
}
EOF

# wait for etcd to become active
while ! curl -sf -o /dev/null http://$FQDN_HOSTNAME:2379/v2/keys/; do
	sleep 1
	echo "Try again"
done

# put the flannel config in etcd
curl -sf -L http://$FQDN_HOSTNAME:2379/v2/keys/flannel/network/config -X PUT --data-urlencode value@/tmp/flannel-network.json
