output "ids" {
  value = ["${oci_core_instance.k8smaster.*.id}"]
}

output "private_ips" {
  value = ["${oci_core_instance.k8smaster.*.private_ip}"]
}

output "public_ips" {
  value = ["${oci_core_instance.k8smaster.*.public_ip}"]
}
