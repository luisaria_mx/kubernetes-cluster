resource "oci_core_instance" "k8smaster" {
  count = "${var.count}"
  availability_domain = "${var.availability_domain}"
  compartment_id = "${var.compartment_ocid}"
  display_name = "${var.k8smaster_display_name_prefix}-ad${substr(var.availability_domain, length(var.availability_domain)-1, -1)}-${count.index}"
  hostname_label = "${var.k8smaster_hostname_label_prefix}-ad${substr(var.availability_domain, length(var.availability_domain)-1, -1)}-${count.index}"
  image = "${lookup(data.oci_core_images.k8smaster_image.images[0], "id")}"
  shape = "${var.k8smaster_instance_shape}"
  subnet_id = "${var.subnet_id}"
  metadata {
    ssh_authorized_keys = "${var.ssh_public_key}"
    user_data           = "${data.template_cloudinit_config.master.rendered}"
    tags                = "group:k8s-master"
  }

  # provisioner "local-exec" {
  #   command = "sleep 900"
  # }

  timeouts {
    create = "60m"
  }
}
