output "k8smaster_instance_ids" {
  value = "${concat(module.k8smasterAD1.ids, module.k8smasterAD2.ids, module.k8smasterAD3.ids)}"
}

output "k8smaster_instance_public_ips" {
  value = "${concat(module.k8smasterAD1.public_ips, module.k8smasterAD2.public_ips, module.k8smasterAD3.public_ips)}"
}

output "k8smaster_instance_private_ips" {
  value = "${concat(module.k8smasterAD1.private_ips, module.k8smasterAD2.private_ips, module.k8smasterAD3.private_ips)}"
}

output "k8sworker_instance_ids" {
  value = "${concat(module.k8sworkerAD1.ids, module.k8sworkerAD2.ids, module.k8sworkerAD3.ids)}"
}

output "k8sworker_instance_public_ips" {
  value = "${concat(module.k8sworkerAD1.public_ips, module.k8sworkerAD2.public_ips, module.k8sworkerAD3.public_ips)}"
}

output "k8sworker_instance_private_ips" {
  value = "${concat(module.k8sworkerAD1.private_ips, module.k8sworkerAD2.private_ips, module.k8sworkerAD3.private_ips)}"
}
